# Kojo Admin (kojo-admin-pwa)

Kojo Admin web app

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```

### API Documentation
See [Kojo Admin API documentation](https://documenter.getpostman.com/view/5954751/T1Dngxgx?version=latest)

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
