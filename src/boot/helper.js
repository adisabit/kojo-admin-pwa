import Vue from 'vue'

function formatRupiah(angka, prefix) {
  let numberString = angka.replace(/[^,\d]/g, '').toString()
  let split = numberString.split(',')
  let remaining = split[0].length % 3
  let rupiah = split[0].substr(0, remaining)
  let ribuan = split[0].substr(remaining).match(/\d{3}/gi)

  if (ribuan) {
      let separator = remaining ? '.' : ''
      rupiah += separator + ribuan.join('.')
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '')
}

Vue.prototype.$formatRupiah = formatRupiah
