import Vue from 'vue'
import moment from 'moment'

moment.locale('id')

Vue.prototype.$moment = moment
