import Vue from 'vue'
import axios from 'axios'

axios.defaults.baseURL = process.env.API_URL
axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.post['Accept'] = 'application/json';

Vue.prototype.$axios = axios
