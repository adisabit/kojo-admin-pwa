
const routes = [
  {
    path: '/',
    component: () => import('layouts/LoginLayout.vue'),
    children: [
      { path: '', component: () => import('pages/login/Index.vue') }
    ]
  },
  {
    path: '/login',
    component: () => import('layouts/LoginLayout.vue'),
    children: [
      { path: '', component: () => import('pages/login/Index.vue') }
    ]
  },
  {
    path: '/dashboard',
    component: () => import('layouts/MainLayout.vue'),
    children: [

      { path: '', component: () => import('pages/Index.vue') }
    ],
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/order',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/order/Index.vue') },
      { path: ':id', component: () => import('pages/order/Detail.vue'), },
      { path: ':id/edit', component: () => import('pages/order/Edit.vue'), },
    ],
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/invoice',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/invoice/Index.vue') },
      { path: 'create', component: () => import('pages/invoice/Create.vue') },
      { path: ':id', component: () => import('pages/invoice/Detail.vue'), },
      { path: ':id/edit', component: () => import('pages/invoice/Edit.vue'), },
    ],
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/clothing',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      // clothing button routes
      { path: 'button', component: () => import('pages/clothing/button/Index.vue') },
      { path: 'button/create', component: () => import('pages/clothing/button/Create.vue') },
      { path: 'button/:id', component: () => import('pages/clothing/button/Detail.vue') },
      { path: 'button/:id/edit', component: () => import('pages/clothing/button/Edit.vue') },

      // jacket type routes
      { path: 'jacket_type', component: () => import('pages/clothing/jacket_type/Index.vue') },
      { path: 'jacket_type/create', component: () => import('pages/clothing/jacket_type/Create.vue') },
      { path: 'jacket_type/:id', component: () => import('pages/clothing/jacket_type/Detail.vue') },
      { path: 'jacket_type/:id/edit', component: () => import('pages/clothing/jacket_type/Edit.vue') },

      // kur rope routes
      { path: 'kur_rope', component: () => import('pages/clothing/kur_rope/Index.vue') },
      { path: 'kur_rope/create', component: () => import('pages/clothing/kur_rope/Create.vue') },
      { path: 'kur_rope/:id', component: () => import('pages/clothing/kur_rope/Detail.vue') },
      { path: 'kur_rope/:id/edit', component: () => import('pages/clothing/kur_rope/Edit.vue') },

      // clothing type routes
      { path: 'type', component: () => import('pages/clothing/type/Index.vue') },
      { path: 'type/create', component: () => import('pages/clothing/type/Create.vue') },
      { path: 'type/:id', component: () => import('pages/clothing/type/Detail.vue') },
      { path: 'type/:id/edit', component: () => import('pages/clothing/type/Edit.vue') },

      // clothing material routes
      { path: 'material', component: () => import('pages/clothing/material/Index.vue') },
      { path: 'material/create', component: () => import('pages/clothing/material/Create.vue') },
      { path: 'material/:id', component: () => import('pages/clothing/material/Detail.vue') },
      { path: 'material/:id/edit', component: () => import('pages/clothing/material/Edit.vue') },

      // clothing puring routes
      { path: 'puring', component: () => import('pages/clothing/puring/Index.vue') },
      { path: 'puring/create', component: () => import('pages/clothing/puring/Create.vue') },
      { path: 'puring/:id', component: () => import('pages/clothing/puring/Detail.vue') },
      { path: 'puring/:id/edit', component: () => import('pages/clothing/puring/Edit.vue') },

      // clothing screen printing routes
      { path: 'screen_printing', component: () => import('pages/clothing/screen_printing/Index.vue') },
      { path: 'screen_printing/create', component: () => import('pages/clothing/screen_printing/Create.vue') },
      { path: 'screen_printing/:id', component: () => import('pages/clothing/screen_printing/Detail.vue') },
      { path: 'screen_printing/:id/edit', component: () => import('pages/clothing/screen_printing/Edit.vue') },

      // clothing stopper routes
      { path: 'stopper', component: () => import('pages/clothing/stopper/Index.vue') },
      { path: 'stopper/create', component: () => import('pages/clothing/stopper/Create.vue') },
      { path: 'stopper/:id', component: () => import('pages/clothing/stopper/Detail.vue') },
      { path: 'stopper/:id/edit', component: () => import('pages/clothing/stopper/Edit.vue') },

      // clothing zipper routes
      { path: 'zipper', component: () => import('pages/clothing/zipper/Index.vue') },
      { path: 'zipper/create', component: () => import('pages/clothing/zipper/Create.vue') },
      { path: 'zipper/:id', component: () => import('pages/clothing/zipper/Detail.vue') },
      { path: 'zipper/:id/edit', component: () => import('pages/clothing/zipper/Edit.vue') },

      // clothing zipper routes
      { path: 'mapping/create', component: () => import('pages/clothing/mapping/Create.vue') }

      /**

      { path: 'price', component: () => import('pages/clothing/price/Index.vue') },
      { path: 'price/create', component: () => import('pages/clothing/price/Create.vue') },
      { path: 'price/type/:clothingTypeId/material/:clothingMaterialId', component: () => import('pages/clothing/price/Detail.vue') },
      { path: 'price/type/:clothingTypeId/material/:clothingMaterialId/edit', component: () => import('pages/clothing/price/Edit.vue') },

      */
    ],
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/request',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'document', component: () => import('pages/request/document/Index.vue') },
      { path: 'document/create', component: () => import('pages/request/document/Create.vue') },
      { path: 'document/:orderId', component: () => import('pages/request/document/Detail.vue') },
      { path: 'document/:orderId/edit', component: () => import('pages/request/document/Edit.vue') },
    ],
    meta: {
      requiresAuth: true
    },
  },
  {
    path: '/disbursement',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      // disbursement for purchase request
      { path: 'cash/purchase_request', component: () => import('pages/disbursement/cash/purchase_request/Index.vue') },
      { path: 'cash/purchase_request/create', component: () => import('pages/disbursement/cash/purchase_request/Create.vue') },
      { path: 'cash/purchase_request/:id', component: () => import('pages/disbursement/cash/purchase_request/Detail.vue') },
      { path: 'cash/purchase_request/:id/edit', component: () => import('pages/disbursement/cash/purchase_request/Edit.vue') },

      // disbursement for expenditure request
      { path: 'cash/expenditure_request', component: () => import('pages/disbursement/cash/expenditure_request/Index.vue') },
      { path: 'cash/expenditure_request/create', component: () => import('pages/disbursement/cash/expenditure_request/Create.vue') },
      { path: 'cash/expenditure_request/:id', component: () => import('pages/disbursement/cash/expenditure_request/Detail.vue') },
      { path: 'cash/expenditure_request/:id/edit', component: () => import('pages/disbursement/cash/expenditure_request/Edit.vue') }
    ],
    meta: {
      requiresAuth: true
    },
  },
  {
    path: '/production',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      // disbursement for purchase request
      { path: 'job_sheet', component: () => import('pages/production/job_sheet/Index.vue') },
      { path: 'job_sheet/create', component: () => import('pages/production/job_sheet/Create.vue') },
      { path: 'job_sheet/:id', component: () => import('pages/production/job_sheet/Detail.vue') },
      { path: 'job_sheet/:id/edit', component: () => import('pages/production/job_sheet/Edit.vue') },

      // disbursement for expenditure request
      { path: 'cash/expenditure_request', component: () => import('pages/disbursement/cash/expenditure_request/Index.vue') },
      { path: 'cash/expenditure_request/create', component: () => import('pages/disbursement/cash/expenditure_request/Create.vue') },
      { path: 'cash/expenditure_request/:id', component: () => import('pages/disbursement/cash/expenditure_request/Detail.vue') },
      { path: 'cash/expenditure_request/:id/edit', component: () => import('pages/disbursement/cash/expenditure_request/Edit.vue') }
    ],
    meta: {
      requiresAuth: true
    },
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
